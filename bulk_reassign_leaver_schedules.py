import looker_sdk #Note that the pip install required a hyphen but the import is an underscore.
import os
import json

os.environ["LOOKERSDK_BASE_URL"] = "https://paybyphone.ca.looker.com"
os.environ["LOOKERSDK_API_VERSION"] = "4.0"
os.environ["LOOKERSDK_VERIFY_SSL"] = "true" #Defaults to true if not set. SSL verification should generally be on unless you have a real good reason not to use it. Valid options: true, y, t, yes, 1.
os.environ["LOOKERSDK_TIMEOUT"] = "120" #Seconds till request timeout. Standard default is 120.

#Get the following values from your Users page in the Admin panel of your Looker instance > Users > Your user > Edit API keys.
os.environ["LOOKERSDK_CLIENT_ID"] =  "XXXXXXXXXXXXX" #No defaults.
os.environ["LOOKERSDK_CLIENT_SECRET"] = "YYYYYYYYYYYYY" #No defaults. This should be protected at all costs. Please do not leave it sitting here.

print("All environment variables set.")

#Initialize the SDK
sdk = looker_sdk.init40()
print('Looker SDK 4.0 initialized successfully.')
print(os.environ["LOOKERSDK_BASE_URL"])

# Return the user id of by looking up email
def find_user_id(email: str):
  user_id = sdk.search_users(email=email)
#   print(user_id)
  if len(user_id) == 0:
    return 'There is no user associated with this email'
  else:
    return int(user_id[0]['id'])

# Return all schedules of a particular user id
def find_schedules(user_id: int):
  result = {}
  schedule_plans = sdk.all_scheduled_plans(user_id=user_id)
#   print(schedule_plans)
  for i in schedule_plans:
    result[i['name']] = i['id']
  return result

def update_owner(current_owner_email: str, new_owner_email: str):
  current_owner_id = find_user_id(current_owner_email)
  new_owner_id = find_user_id(new_owner_email)
  
  if type(current_owner_id) != int and type(new_owner_id) != int:
    return "The email addresses for both the current owner and the new owner are not associatd with any Looker user id"

  elif type(current_owner_id) != int: 
    return "The email address for the current owner is not associated with any Looker user id"

  elif type(new_owner_id) != int:
    return "The email address for the new owner is not associated with any Looker user id"

  else: 
    body = {}
    body['user_id'] = new_owner_id
    find = find_schedules(current_owner_id) 
    print(find)
    for i in find.values(): 
        sdk.update_scheduled_plan(i,body)

    if find == find_schedules(new_owner_id) and find_schedules(current_owner_id) == {}: 

      result = "Successfully transfer the following schedules from " + current_owner_email + " to " + new_owner_email
      print(result)
      for i in find: 
        print(i)
      return None
      
    else: 
      return "Something went wrong, please check if email is correct or if the user has any schedules."

update_owner('test@paybyphone.com', 'test2@paybyphone.com')