import looker_sdk #Note that the pip install required a hyphen but the import is an underscore.
import os
import json

os.environ["LOOKERSDK_BASE_URL"] = "https://paybyphone.ca.looker.com" 
os.environ["LOOKERSDK_API_VERSION"] = "4.0" 
os.environ["LOOKERSDK_VERIFY_SSL"] = "true" #Defaults to true if not set. SSL verification should generally be on unless you have a real good reason not to use it. Valid options: true, y, t, yes, 1.
os.environ["LOOKERSDK_TIMEOUT"] = "120" #Seconds till request timeout. Standard default is 120.

#Get the following values from your Users page in the Admin panel of your Looker instance > Users > Your user > Edit API keys.
os.environ["LOOKERSDK_CLIENT_ID"] =  "XXXXXXXXXXXXXX" #No defaults.
os.environ["LOOKERSDK_CLIENT_SECRET"] = "YYYYYYYYYYYYYY" #No defaults. This should be protected at all costs. Please do not leave it sitting here.

print("All environment variables set.")

#Initialize the SDK
sdk = looker_sdk.init40()
print('Looker SDK 4.0 initialized successfully.')
print(os.environ["LOOKERSDK_BASE_URL"])

# Return the user id of by looking up email
def find_user_id(email: str):
  user_id = sdk.search_users(email=email)
  if len(user_id) == 0:
    return 'There is no user associated with this email'
  else:
    return int(user_id[0]['id'])

# Return all schedules of a particular user id
def find_schedules(user_id: int):
  result = {}
  schedule_plans = sdk.all_scheduled_plans(user_id=user_id)
  for i in schedule_plans:
    result[i['id']] = {
            'name': i['name'],
            'crontab': i['crontab']
    }
  return result

def update_schedule(current_owner_email: str, current_crontab: str, new_crontab: str, limit: int = 120):
    current_owner_id = find_user_id(current_owner_email)
    body = {}
    body['crontab'] = new_crontab
    schedules = find_schedules(current_owner_id)     
    count = 0
    found = False
    for schedule_id, schedule_info in schedules.items():
        if schedule_info['crontab'] == current_crontab:
#             print(schedule_info)
            found = True
            try:
                sdk.update_scheduled_plan(schedule_id, body)
                updated_schedule = sdk.scheduled_plan(schedule_id)
                print(f"Success: Updated Schedule ID: {updated_schedule['id']}, Name: {updated_schedule['name']}, Old Crontab: {current_crontab} New Crontab: {updated_schedule['crontab']}")
                count += 1
                if count >= limit:
                    break
            except Exception as e:
                print(f"Failure: Could not update Schedule ID: {schedule_id}, Name: {schedule_info['name']}. Error: {str(e)}")

    if not found:
        print(f"No schedules found with crontab: {current_crontab}")

update_schedule('test@paybyphone.com', '0 6 1 * *', '0 7 1 * *')